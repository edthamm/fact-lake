from time import sleep
from fact_lake.config import get_logger
import pytest

from pathlib import Path
from os import environ as env
from testcontainers.compose import DockerCompose

log = get_logger("environments")


@pytest.fixture(scope="module")
def local_factcast_compose():
    environment = DockerCompose(
        Path(__file__).parent.parent.absolute().joinpath("resources")
    )

    if env.get("CI_JOB_STAGE", False):
        environment = DockerCompose(
            Path(__file__).parent.parent.absolute().joinpath("resources"),
            compose_file_name="docker-compose-ci.yml",
        )

    environment.start()

    timeout = 120
    while timeout > -1:
        stdout, _ = environment.get_logs()
        if "Started FactCastServer in" in str(stdout):  # TODO better signal
            break
        sleep(1)
        timeout -= 1
    else:
        stdout, stderr = environment.get_logs()
        log.error(
            f"Timeout reached waiting for factcast to start. Compose STDOUT: {stdout} Compose STDERR: {stderr}"
        )

    yield

    environment.stop()
